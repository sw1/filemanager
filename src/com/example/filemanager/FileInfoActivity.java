package com.example.filemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FileInfoActivity extends Activity {
	TextView textTitle;
	ListView listInfo;
	ProgressBar progressBar;
	ImageView icon;
	Map<String, String> fileInfo;
	ArrayList<String> fileInfoList;
	ArrayAdapter<String> adapterInfo;
	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_info);

		fileInfo = new HashMap<String, String>();
		fileInfoList = new ArrayList<String>();

		textTitle = (TextView) findViewById(R.id.textTitle);
		listInfo = (ListView) findViewById(R.id.listInfo);
		icon = (ImageView) findViewById(R.id.icon);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		
		adapterInfo = new ArrayAdapter<String>(this, R.layout.list_item,
				fileInfoList);
		listInfo.setAdapter(adapterInfo);

		Bundle extras = getIntent().getExtras();
		file = new File(extras.getString("file"));

		new fileInfoThread().execute();
	}

	private class fileInfoThread extends AsyncTask<Void, Void, Void> {
		String mimeTypeThread;
		long sizeThread;
		File fileThread;
		
		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(ProgressBar.VISIBLE);
			mimeTypeThread = fileUtils.getFileType(file);
			textTitle.setText(file.getName());
			icon.setImageResource(fileUtils.getMimeIcon(mimeTypeThread));
			fileInfo.put("type", mimeTypeThread);
			fileInfo.put("last modified",
					fileUtils.getLastModificationTime(file, "MM-dd-yyyy"));
			sizeThread = 0;
			fileThread = file;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			sizeThread= fileThread.isDirectory() ? fileUtils.getDirSize(fileThread) : fileThread
					.length();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			fileInfo.put("size", fileUtils.formatSize(sizeThread));
			for (Entry<String, String> x : fileInfo.entrySet())
				fileInfoList.add(x.getKey() + ": " + x.getValue());
			progressBar.setVisibility(ProgressBar.INVISIBLE);
			adapterInfo.notifyDataSetChanged();
		}
		
	}
}
