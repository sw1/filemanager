package com.example.filemanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.net.Uri;

public class fileUtils {
	static enum SortType {
		NONE, NAME, NAME_R, SIZE, SIZE_R, TYPE, TYPE_R, DATE, DATE_R
	}

	static ArrayList<File> searchResults;

	public static void clearSearchList() { // TODO: DELETE THIS
		searchResults = new ArrayList<File>();
	}

	public static ArrayList<File> searchInDir(String query, File dir) {
		File[] files = dir.listFiles();
		if (files != null)
			for (File file : files)
				if (file.isDirectory()) {
					if (file.getName().contains(query))
						searchResults.add(file);
					searchInDir(query, file);
				} else if (file.getName().contains(query))
					searchResults.add(file);
		return searchResults;
	}

	private static Comparator<File> alphaComparator = new Comparator<File>() {
		@Override
		public int compare(File lhs, File rhs) {
			return lhs.getName().compareToIgnoreCase(rhs.getName());
		}
	};
	private static Comparator<File> sizeComparator = new Comparator<File>() {
		@Override
		public int compare(File lhs, File rhs) {
			return Long.valueOf(lhs.length()).compareTo(rhs.length());
		}
	};
	private static Comparator<File> dateComparator = new Comparator<File>() {
		@Override
		public int compare(File lhs, File rhs) {
			return Long.valueOf(lhs.lastModified()).compareTo(
					rhs.lastModified());
		}
	};
	private static Comparator<File> typeComparator = new Comparator<File>() {
		@Override
		public int compare(File lhs, File rhs) {
			return getFileType(lhs).compareToIgnoreCase(getFileType(rhs));
		}
	};

	public static int getMimeIcon(String mimeType) {
		if (mimeType.equals("directory")) {
			return R.drawable.directory;
		} else if (mimeType.equals("text/html")) {
			return R.drawable.text_html;
		} else if (mimeType.startsWith("text")) {
			return R.drawable.text_generic;
		} else if (mimeType.startsWith("image")) {
			return R.drawable.image_x_generic;
		} else if (mimeType.startsWith("audio")) {
			return R.drawable.audio_generic;
		} else if (mimeType.startsWith("video")) {
			return R.drawable.video_generic;
		} else if (mimeType.equals("application/pdf")) {
			return R.drawable.application_pdf;
		} else if (mimeType.equals("application/vnd.android.package-archive")) {
			return R.drawable.application_executable;
		}
		return R.drawable.unknown;

	}

	public static void sort(ArrayList<File> fileList, SortType sortType) {
		Comparator<File> comparator = alphaComparator;
		if (sortType == null)
			sortType = SortType.NAME;
		if (sortType.equals(SortType.NAME))
			comparator = alphaComparator;
		else if (sortType.equals(SortType.SIZE))
			comparator = sizeComparator;
		else if (sortType.equals(SortType.TYPE))
			comparator = typeComparator;
		else if (sortType.equals(SortType.DATE))
			comparator = dateComparator;

		ArrayList<File> dirs = new ArrayList<File>();
		ArrayList<File> files = new ArrayList<File>();

		for (File file : fileList)
			if (file.isDirectory())
				dirs.add(file);
			else
				files.add(file);

		Collections.sort(dirs, alphaComparator);

		Collections.sort(files, comparator);

		fileList.clear();
		fileList.addAll(dirs);
		fileList.addAll(files);
	}

	public static String getExtension(String filename) {
		String extension = null;
		int index = filename.lastIndexOf('.');
		if (index > 0 && index < filename.length() - 1)
			extension = filename.substring(index + 1).toLowerCase(Locale.US);
		return extension;
	}

	public static void reverse(ArrayList<File> fileList) {
		ArrayList<File> files = new ArrayList<File>();
		ArrayList<File> dirs = new ArrayList<File>();

		for (File file : fileList)
			if (file.isDirectory())
				dirs.add(file);
			else
				files.add(file);

		Collections.reverse(dirs);
		Collections.reverse(files);

		fileList.clear();
		fileList.addAll(dirs);
		fileList.addAll(files);
	}

	public static String getFileType(File file) {
		if (file.isDirectory()) {
			return "directory";
		} else {
			Uri uri = Uri.fromFile(file);

			String mimeType = URLConnection.guessContentTypeFromName(uri
					.toString());

			return ((mimeType == null) ? "unknown" : mimeType);
		}
	}

	public static long getDirSize(File file) {
		long size = 0;
		for (File f : file.listFiles())
			if (f.isDirectory())
				size += getDirSize(f);
			else
				size += f.length();

		return size;
	}

	public static String formatSize(long size) {
		if (size < 1024)
			return Long.toString(size) + " bytes";
		if (size < Math.pow(1024, 2))
			return String.format("%.3g", (double) size / 1024) + " kB";
		if (size < Math.pow(1024, 3))
			return String.format("%.3g", (double) size / Math.pow(1024, 2))
					+ " MB";
		if (size < Math.pow(1024, 4))
			return String.format("%.3g", (double) size / Math.pow(1024, 3))
					+ " GB";
		if (size < Math.pow(1024, 5))
			return String.format("%.3g", (double) size / Math.pow(1024, 4))
					+ " TB";
		return "BŁĄD";
	}

	@SuppressLint("SimpleDateFormat")
	public static String getLastModificationTime(File file, String format) {
		return new SimpleDateFormat(format)
				.format(new Date(file.lastModified()));
	}

	@SuppressLint("NewApi")
	public static void copy(File src, File dest) throws IOException {
		if (src.isDirectory()) {
			if (dest.mkdirs() == false)
				throw new IOException("Can't create directory: "
						+ dest.getAbsolutePath());
			if (src.listFiles() != null) {
				for (File file : src.listFiles()) {
					copy(file,
							new File(dest.getAbsolutePath() + "/"
									+ file.getName()));
				}
			}
		} else {
			FileInputStream in = null;
			FileOutputStream out = null;
			in = new FileInputStream(src);
			out = new FileOutputStream(dest);
			byte buf[] = new byte[1024];
			while (in.read(buf) > 0) {
				out.write(buf);
			}
			in.close();
			out.close();
		}
	}

	public static boolean remove(File src) {
		boolean suceed = true;
		if (src.isDirectory()) {
			if (src.listFiles() != null) {
				for (File file : src.listFiles()) {
					if (!remove(file)) {
						suceed = false;
					}
				}
			}
		}
		if (!src.delete()) {
			suceed = false;
		}
		return suceed;
	}

	public static void cut(File src, File dest) throws IOException {
		copy(src, dest);
		remove(src);
	}

	public static void copy(ArrayList<File> src, File dest) throws IOException {
		for (File file : src) {
			copy(file, new File(dest.getAbsolutePath() + "/" + file.getName()));
		}
	}

	public static boolean remove(ArrayList<File> src) {
		boolean suceed = true;
		for (File file : src) {
			if (!remove(file)) {
				suceed = false;
			}
		}
		return suceed;
	}

	public static void cut(ArrayList<File> src, File dest) throws IOException {
		for (File file : src) {
			copy(file, new File(dest.getAbsolutePath() + "/" + file.getName()));
			remove(file);
		}
	}

}
